.PHONY:clean test

CC=g++

test.app:main.o rule_creator.o
	$(CC) *.o -o test.app -g3
%.o:%.cpp
	$(CC) -c $< -o $@ -g3

test:
	./test.app
clean:
	rm *.o *.app ./rules/*
