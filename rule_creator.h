#ifndef _RULE_CREATOR_
#define _RULE_CREATOR_

#include <stdint.h>
#include <vector>
#include <set>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>//errno
#include <unistd.h>//getopt()
#include <time.h>//time()

#define MAX_FILES_NB 32
#define MAX_ADDR_TYPE 2
#define IPV4_ADDR_LEN 4
#define IPV6_ADDR_LEN 8


#define OUT_FORMAT(MODE)\
	(MODE==0 ? "%x" : "%lx")

struct ipv4_addr{
	uint32_t addr;
	uint16_t bits_len;

	bool operator==(const ipv4_addr& tmp) const
	{
		return (this->addr == tmp.addr && this->bits_len == tmp.bits_len);
	}

	bool operator>(const ipv4_addr& tmp) const
	{
		return (this->addr > tmp.addr || (this->addr == tmp.addr && this->bits_len > tmp.bits_len) );
	}

	bool operator<(const ipv4_addr& tmp) const
	{
		return (this->addr < tmp.addr || (this->addr == tmp.addr && this->bits_len < tmp.bits_len) );
	}
};

struct ipv6_addr{
	uint64_t addr;
	uint16_t bits_len;
	
	bool operator==(const ipv6_addr& tmp) const
	{
		return (this->addr == tmp.addr && this->bits_len == tmp.bits_len);
	}

	bool operator>(const ipv6_addr& tmp) const
	{
		return (this->addr > tmp.addr || (this->addr == tmp.addr && this->bits_len > tmp.bits_len) );
	}

	bool operator<(const ipv6_addr& tmp) const
	{
		return (this->addr < tmp.addr || (this->addr == tmp.addr && this->bits_len < tmp.bits_len) );
	}
};

struct segment_t {
  uint32_t ot : 1;
  uint32_t lt : 1;
  uint32_t oft  : 11; // in octets
  uint32_t len  : 11; // in octets
  uint32_t mask : 8;  // tail mask
};

struct rule_t {
  std::vector<segment_t> segments;
  std::vector<uint8_t> key;
  uint32_t priority;
  uint32_t action;
};

void parse_args(int argc, char **argv);
void print_usage();
void read_all_addr_file();
int read_addr_data(FILE * fp,  int type, int id);
int read_addr_data_raw(FILE * fp, uint32_t * res, uint16_t *bits_len);
int read_addr_data_raw(FILE * fp, uint64_t * res, uint16_t *bits_len);
void rand_select_vec_id(uint32_t vec_id[], int target_nb);

void create_rules_v4(int rule_nb);
void write_one_rule_v4(FILE * fp, std::vector<ipv4_addr> & addr_vec);

void create_rules_v6(int rule_nb);
void write_one_rule_v6(FILE * fp, std::vector<ipv6_addr> & addr_vec);

#endif
