#include "rule_creator.h"

int main(int argc, char ** argv){
	
	//parse args
	parse_args(argc, argv);	

	//read all addr file : ipv4, ipv6 ...
	read_all_addr_file();

	printf("start to create rule\n");
	//create rules for each address type
	//<addr>, <addr, addr>, <addr, addr, addr>, ...
	for(int i=0; i<MAX_ADDR_TYPE; i++){
		create_rules_v4(i);
		create_rules_v6(i);
	}	


	return 0;
}
