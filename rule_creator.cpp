#include "rule_creator.h"

//global addr data vector
std::vector<ipv4_addr> ipv4_v[MAX_FILES_NB];
std::vector<ipv6_addr> ipv6_v[MAX_FILES_NB];

//address type
int addr_type[MAX_ADDR_TYPE]={4, 6};

//number of rules for each rule type
int rules_nb = 0;

//number of files for each address type
int files_nb = 0;

void parse_args(int argc, char **argv){
	int opt;

	if(argc < 3)
	{
		print_usage();
		exit(1);
	}	
	while( (opt = getopt(argc, argv, "n:N:")) != -1){
		switch(opt){
			case 'n':
				files_nb = atoi(optarg);
				printf("%d files for each address type\n", files_nb);
				break;
			case 'N':
				rules_nb = atoi(optarg);
				printf("%d rules for each rule type\n", rules_nb);
				break;
			default:
				print_usage();
				exit(1);
		}
	}
	if(files_nb <= 0 || rules_nb <=0){
		print_usage();
		exit(1);
	}
}

void print_usage(){
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t -n : number of file for each address type\n");
	fprintf(stderr, "\t -N : number of rules for each rule type\n");
}

/*
	addr file name format
		./ip_set/[id].v[ip_version]
	id : 00, 01, 02, 03 ....
	ip_version : 4, 6 
*/
void read_all_addr_file(){
	char file_path[100]="";
	FILE * fp;
	int i=0 ,j=0;

	for(i = 0; i < files_nb; i++){
		for(j=0; j < MAX_ADDR_TYPE; j++){
			sprintf(file_path, "./ip_set/%.2d.v%d", i, addr_type[j]);
			fp = fopen(file_path, "r");

			//check if addr file open is open
			if(fp == NULL){
				fprintf(stderr, "\"%s\" open error:%s\n", file_path, strerror(errno));
				exit(1);
			}		

			//read addr data
			read_addr_data(fp, j, i);

			//close addr file
			fclose(fp);
		}
	}		
}

int read_addr_data(FILE * fp, int type, int id){
	if(type == 0){//ipv4
		uint32_t res;
		uint16_t bits_len;	
		ipv4_addr ia_v4;
		std::set<ipv4_addr> set_v4;	
		std::insert_iterator<std::vector<ipv4_addr> > ins_it_v4(ipv4_v[id], ipv4_v[id].begin());		
	
		while(read_addr_data_raw(fp, &res, &bits_len)){
			ia_v4.addr = res;
			ia_v4.bits_len = bits_len;
			set_v4.insert(ia_v4);				
		}
		
		//copy from set to vector
		copy(set_v4.begin(), set_v4.end(), ins_it_v4);
	}
	else{//ipv6
		uint64_t res;
		uint16_t bits_len;	
		ipv6_addr ia_v6;
		std::set<ipv6_addr> set_v6;
		std::insert_iterator<std::vector<ipv6_addr> > ins_it_v6(ipv6_v[id], ipv6_v[id].begin());		
	
		while(read_addr_data_raw(fp, &res, &bits_len)){
			ia_v6.addr = res;
			ia_v6.bits_len = bits_len;
			set_v6.insert(ia_v6);
		}

		//copy from set to vector
		copy(set_v6.begin(), set_v6.end(), ins_it_v6);
	}
}

int read_addr_data_raw(FILE * fp, uint32_t * res, uint16_t *bits_len){
	
	if( fscanf(fp, "%X/%hu", res, bits_len) != EOF ){
		return 1;
	}
	else
		return 0;
}

int read_addr_data_raw(FILE * fp, uint64_t * res, uint16_t *bits_len){
	
	if( fscanf(fp, "%lX/%hu", res, bits_len) != EOF){
		return 1;
	}
	else
		return 0;
}

void rand_select_vec_id(uint32_t vec_id[], int tar_nb){
	uint32_t src_base[MAX_FILES_NB];	
	uint32_t i = 0 ;
	uint32_t count = 0;
	uint32_t cur_id;
	uint32_t temp;

	for(i=0; i<files_nb; i++){
		src_base[i] = i;
	}
	
	//if target nb is equal to files_nb, all vectors must be selected
	if(tar_nb == files_nb){
		for(i=0; i<files_nb; i++){
			vec_id[i] = i;
		}
		
		return;
	}	

	//select tar_nb from files_nb vectors
	while(count < tar_nb){
		cur_id = rand() % (files_nb - count);
		temp = src_base[files_nb - count - 1];
		src_base[files_nb - count - 1] = src_base[cur_id];
		src_base[cur_id] = temp;
		count++;
	}
	
	for(i=0; i<tar_nb; i++){
		vec_id[i] = src_base[files_nb - 1 - i];
	}
}

void create_rules_v4(int type){
	int i, j, k;
	char file_path[50];
	int version[2]={4, 6};
	FILE * fp_out;
	uint64_t count=0;
	uint32_t select_id_vec[MAX_FILES_NB];
	uint32_t select_id;

	//create random seed	
	srand((unsigned)time(NULL));

	for(i=1; i<=files_nb; i++){

		//prepare out file
		sprintf(file_path, "./rules/%d.v4", i);		
		fp_out = fopen(file_path, "w");
		if(fp_out == NULL){
			fprintf(stderr, "\"%s\" open error:%s\n", file_path, strerror(errno));
			exit(1);
		}

		//current rule file set
		std::set<std::vector<ipv4_addr> > cur_rule_set;
	
		//select i vectors from all randomly
		rand_select_vec_id(select_id_vec, i);

		count = 0;
	
		while(count < rules_nb){

			std::vector<ipv4_addr> cur_rule;

			for(j=0; j<i; j++){
				select_id = select_id_vec[j];

				ipv4_addr & cur_addr = ipv4_v[select_id][rand() % ipv4_v[select_id].size()];							

				cur_rule.push_back(cur_addr);	
			}				

			//check if it is in set					
			if(cur_rule_set.find(cur_rule) == cur_rule_set.end()){
				count++;

				cur_rule_set.insert(cur_rule);
	
				write_one_rule_v4(fp_out, cur_rule);									
			}
		}
	
		fclose(fp_out);
	}			

}

void write_one_rule_v4(FILE * fp, std::vector<ipv4_addr> & addr_vec){
	std::vector<uint8_t> key;
	int total_byte_len = 0;
	segment_t seg_temp;

	
	//priority
	fprintf(fp, "%d ", rand() % 65536);
	
	//action
	fprintf(fp, "%d ", 0);

	//segments number
	fprintf(fp, "%lu ", addr_vec.size() + 1);

	//write ether type segment first
	seg_temp.ot = 0;
	seg_temp.lt = 0;
	seg_temp.oft = 12;
	seg_temp.len = 2;
	seg_temp.mask = 0xff;
	key.push_back(0x08);
	key.push_back(0x00);
	fprintf(fp, "%u ", *((uint32_t *)(&seg_temp)));
	total_byte_len += seg_temp.len;
	seg_temp.oft += seg_temp.len; 

	//write address segment	
	int mask_zero_nb;
	uint8_t mask_seed = 1;
	uint8_t * key_byte = NULL;
	for(int i=0; i<addr_vec.size(); i++){
		ipv4_addr & cur_addr = addr_vec[i];
		
		seg_temp.len = (cur_addr.bits_len + 7) / 8;
		
		//mask_gap is number of zero, it is 4 for "11110000"
		mask_zero_nb = seg_temp.len * 8 - cur_addr.bits_len;
	
		seg_temp.mask = 0xff;
		for(int j=0; j<mask_zero_nb; j++){
			seg_temp.mask &= ~(mask_seed<<j);
		}
		
	    key_byte = (uint8_t*)(&cur_addr.addr);
		for(int j=0; j<seg_temp.len; j++){
			if(j == seg_temp.len - 1)
				key.push_back( *(key_byte + j) & seg_temp.mask);
			else
				key.push_back( *(key_byte + j));
		}
		total_byte_len += seg_temp.len;
		fprintf(fp, "%u ", *((uint32_t *)(&seg_temp)));
		seg_temp.oft += IPV4_ADDR_LEN;			
	}
	
	//write key data
	//key numbers
	fprintf(fp, "%d ", total_byte_len);

	for(int i=0; i<key.size(); i++){
		fprintf(fp, "%.2x", (uint8_t)key[i]);
	}		
	fprintf(fp, "\n");
}

void create_rules_v6(int type){
	int i, j, k;
	char file_path[50];
	int version[2]={4, 6};
	FILE * fp_out;
	uint64_t count=0;
	uint32_t select_id_vec[MAX_FILES_NB];
	uint32_t select_id;

	//create random seed	
	srand((unsigned)time(NULL));

	for(i=1; i<=files_nb; i++){

		//prepare out file
		sprintf(file_path, "./rules/%d.v6", i);		
		fp_out = fopen(file_path, "w");
		if(fp_out == NULL){
			fprintf(stderr, "\"%s\" open error:%s\n", file_path, strerror(errno));
			exit(1);
		}

		//current rule file set
		std::set<std::vector<ipv6_addr> > cur_rule_set;
	
		//select i vectors from all randomly
		rand_select_vec_id(select_id_vec, i);

		count = 0;
	
		while(count < rules_nb){

			std::vector<ipv6_addr> cur_rule;

			for(j=0; j<i; j++){
				select_id = select_id_vec[j];

				ipv6_addr & cur_addr = ipv6_v[select_id][rand() % ipv6_v[select_id].size()];							

				cur_rule.push_back(cur_addr);	
			}				

			//check if it is in set					
			if(cur_rule_set.find(cur_rule) == cur_rule_set.end()){
				count++;

				cur_rule_set.insert(cur_rule);
	
				write_one_rule_v6(fp_out, cur_rule);									
			}
		}
	
		fclose(fp_out);
	}			

}

void write_one_rule_v6(FILE * fp, std::vector<ipv6_addr> & addr_vec){
	std::vector<uint8_t> key;
	int total_byte_len = 0;
	segment_t seg_temp;

	
	//priority
	fprintf(fp, "%d ", rand() % 65536);
	
	//action
	fprintf(fp, "%d ", 0);

	//segments number
	fprintf(fp, "%lu ", addr_vec.size() + 1);

	//write ether type segment first
	seg_temp.ot = 0;
	seg_temp.lt = 0;
	seg_temp.oft = 12;
	seg_temp.len = 2;
	seg_temp.mask = 0xff;
	key.push_back(0x08);
	key.push_back(0x00);
	fprintf(fp, "%u ", *((uint32_t *)(&seg_temp)));
	total_byte_len += seg_temp.len;
	seg_temp.oft += seg_temp.len; 

	//write address segment	
	int mask_zero_nb;
	uint8_t mask_seed = 1;
	uint8_t * key_byte = NULL;
	for(int i=0; i<addr_vec.size(); i++){
		ipv6_addr & cur_addr = addr_vec[i];
		
		seg_temp.len = (cur_addr.bits_len + 7) / 8;
		
		//mask_gap is number of zero, it is 4 for "11110000"
		mask_zero_nb = seg_temp.len * 8 - cur_addr.bits_len;
	
		seg_temp.mask = 0xff;
		for(int j=0; j<mask_zero_nb; j++){
			seg_temp.mask &= ~(mask_seed<<j);
		}
		
	    key_byte = (uint8_t*)(&cur_addr.addr);
		for(int j=0; j<seg_temp.len; j++){
			if(j == seg_temp.len - 1)
				key.push_back( *(key_byte + j) & seg_temp.mask);
			else
				key.push_back( *(key_byte + j));
		}
		total_byte_len += seg_temp.len;
		fprintf(fp, "%u ", *((uint32_t *)(&seg_temp)) );
		seg_temp.oft += IPV6_ADDR_LEN;			
	}
	
	//write key data
	//key numbers
	fprintf(fp, "%d ", total_byte_len);

	for(int i=0; i<key.size(); i++){
		fprintf(fp, "%.2x", (uint8_t)key[i]);
	}		
	fprintf(fp, "\n");
}
